//CRUD Operations
/*
	Create - insert


*/
	
// NOTE: f5 + control + enter


// Inserting documnets (create)
// Syntax: db.collectionName.insertOne({object});
// Javascript: object.object.method({object});

db.users.insertOne({
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
            "phone": "09196543410",
            "email": "janedoe@mail.com"            
            },
         "courses": ["CSS", "Javascript", "python"],
         "department": "none"    
    });

// Insert Many
// Syntax: db.collectionName.insertMany([{objectA},{objectB}]);


db.users.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
            "phone": "09179876543",
            "email": "stephenhawking@mail.com"            
            },
         "courses": ["React", "PHP", "python"],
         "department": "none"    
    }, 
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
            "phone": "09061234567",
            "email": "neilarmstrong@mail.com"            
            },
         "courses": ["React", "Laravel", "SASS"],
         "department": "none"    
    }
    ]);


// finding Documents
/* Find Syntax:
	db.collectionName.find();
	db.collectionName.find({field: value});
	*/

	db.users.find
	db.users.find({"lastName": "Doe"});
	db.users.find({"lastName": "Doe", "age": 25
	}).pretty();

	// Updating Documents (Update) Operation
	// Syntax:db.collectionName.update({criteria}, {$set: {field: value}});

	// Insert test documents

	
	db.users.updateOne({
    "firstName": "Test",
        "lastName": "Test",
        "age": 0,
        "contact": {
            "phone": "00000000000",
            "email": "test@mail.com"            
            },
         "courses": [],
         "department": "none"    
    });


    // Update one document

    db.users.updateOne(
    {"firstName": "Test"},
    {    
        $set:{
    "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "contact": {
            "phone": "0017123465",
            "email": "bill@mail.com"            
            },
         "courses": ["PHP", "Laravel", "HTML"],
         "department": "none"    
    
        }     
       }
 );
    // Update multiple documents
    db.users.updateMany(
    {"department": "none"},
    {
            $set: {"department" : "HR"}
     }
 );


// Deleting Documents (Delete) Operation
// Syntax: db.collectionName.deleteOne({criteria}); ~ deleting single document

// Create a single document with One field
db.users.insert({
    "firstName": "test"
    
    });   

// Delete one document
db.users.deleteOne({
    "firstName": "test"  
    });


// Delete multiple documents

db.users.deleteMany({
    "department": "HR"  
});

// Query of embeded document

db.users.find({
	"contact": {
		"phone": "0017123465",
		"email": "bill@mail.com"

	}

});

// Query an Array without a specific order of elements

db.users.find({"courses": { $all:["React", "Python"]}});

